package com.examples.serviceproviders;

public interface MessageServiceProvider {
	void sendMessage(String message);
}
