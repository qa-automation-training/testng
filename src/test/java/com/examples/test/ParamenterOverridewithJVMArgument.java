package com.examples.test;


    import org.testng.Assert;
    import org.testng.annotations.Parameters;
    import org.testng.annotations.Test;

    public class ParamenterOverridewithJVMArgument {

      @Test
      @Parameters("name")
      public void testMethod(String name) {
        Assert.assertEquals(name, System.getProperty("name"));
      }
    }

