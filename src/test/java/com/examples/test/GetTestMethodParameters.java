package com.examples.test;

import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GetTestMethodParameters {

  @BeforeMethod
  public void beforeMethod(ITestResult result) {
    Object[] parameters = result.getParameters();
    System.out.println("paremeters:"+parameters);
    if (parameters == null || parameters.length == 0) {
      throw new IllegalStateException("parameters arent visible");
    }
  }

  @Test(dataProvider = "dp")
  public void testMethod(int i) {}

  @DataProvider(name = "dp")
  public Object[][] getData() {
    return new Object[][] {{1}, {2}};
  }
}

