package abc;

import org.testng.IHookCallBack;
import org.testng.IHookable;
import org.testng.ITestResult;

public class IHookableListener implements IHookable{

	@Override
	public void run(IHookCallBack callBack, ITestResult testResult) {
		callBack.runTestMethod(testResult);
		int retryCount = 0;// TODO Auto-generated method stub
		for (int i = 0; i <= 3; i++) {
			if (testResult.getThrowable() != null) {
				callBack.runTestMethod(testResult);
				System.out.println("RETRY COUNT" + ++retryCount);
			} else if (testResult.getThrowable() == null) {
				break;
			}
		}
	}

}
