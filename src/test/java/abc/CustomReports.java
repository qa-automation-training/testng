package abc;

import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.xml.XmlSuite;

public class CustomReports implements IReporter{
    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
        //Iterating over each suite included in the test
       for(ISuite suite:suites) {
          String suiteName=suite.getName();
          Map<String, ISuiteResult> suiteResults=suite.getResults();
          System.out.println(suiteResults);//getting result of the suite
          for(ISuiteResult sr: suiteResults.values())
          {
              ITestContext tc=sr.getTestContext();
              System.out.println("passed tests for :"+ suiteName+ ":"+tc.getPassedTests().getAllResults().size());
              System.out.println("failed tests for :"+ suiteName+ ":"+tc.getFailedTests().getAllResults().size());
              System.out.println("skipped tests for :"+ suiteName+ ":"+tc.getSkippedTests().getAllResults().size());
          }
       }
    }

}
