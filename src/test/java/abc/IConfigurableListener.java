package abc;

import org.testng.IConfigurable;
import org.testng.IConfigureCallBack;
import org.testng.ITestResult;

public class IConfigurableListener implements IConfigurable {

	@Override
	public void run(IConfigureCallBack callBack, ITestResult testResult) {
		callBack.runConfigurationMethod(testResult);
		int retryCount = 0;// TODO Auto-generated method stub
		for (int i = 0; i <= 3; i++) {
			if (testResult.getThrowable() != null) {
				callBack.runConfigurationMethod(testResult);
				System.out.println("RETRY COUNT" + ++retryCount);
			} else if (testResult.getThrowable() == null) {
			    break;
			}
		}
	}

}
